.syntax unified
.text
.align 
.global	main	
.equ D 1000000000

main:
	push {lr}   
    
	ldr r1, =num1
	ldr r0, =scan_format

	bl scanf	@ read number into num1
    
    bl int2dec
    
	ldr r1,[r0]
    ldr r0,=outmsg
	bl printf

	pop { pc}
    
int2dec:
    push {lr}
    
    @ inicializa os registros
    mov r2, D
    mov r4, #0
    
    loop:
        udiv r3, r0, D      @ r3 = r0/D
        strb r3, [r1,r4]    @ r1[r4] = r3
        add r4, r4, #1      @ r4++
        mul r3, r3, D       @ r3 *= D
        sub r0, r0, r3      @ r0 -= r3
        cmp r4, #10
        blt loop            @ if(r4 < 10) goto loop
    
    pop {pc}
    
scan_format:	.asciz "%d"

outmsg: .asciz "Saida:%s\n"
.data
.align	
num1: .word 0
string: .space 11  @ um espaco adicional para o '\0'
.end
